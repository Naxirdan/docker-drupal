FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -yq --no-install-recommends \
    apt-utils \
    curl \
    # Instalar git
    git \
    # Instalar apache
    apache2 \
    # Instalar php 7.2
    libapache2-mod-php7.2 \
    php7.2-cli \
    php7.2-json \
    php7.2-curl \
    php7.2-fpm \
    php7.2-gd \
    php7.2-ldap \
    php7.2-mbstring \
    php7.2-mysql \
    php7.2-soap \
    php7.2-sqlite3 \
    php7.2-xml \
    php7.2-zip \
    php7.2-intl \
    php-imagick \
    # Instalar tools
    openssl \
    nano \
    ca-certificates \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# Instalar composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Configurar PHP
COPY prueba.ini /etc/php/7.2/mods-available/
RUN phpenmod prueba
# Configurar apache
RUN a2enmod rewrite expires
RUN echo "ServerName localhost" | tee /etc/apache2/conf-available/servername.conf
RUN a2enconf servername
# Configurar vhost
COPY prueba.conf /etc/apache2/sites-available/
RUN a2dissite 000-default
RUN a2ensite prueba.conf

EXPOSE 80 443

WORKDIR /var/www/html

RUN composer global require drush/drush

ENV PATH="~/.composer/vendor/bin:${PATH}"

RUN rm index.html

HEALTHCHECK --interval=5s --timeout=3s --retries=3 CMD curl -f http://localhost || exit 1

CMD apachectl -D FOREGROUND 